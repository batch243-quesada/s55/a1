// modules
import React from 'react'
import { Col, Row, Button } from 'react-bootstrap';

const Banner = () => {
  return (
    <Row className='mt-3 mb-5'>
        <Col>
            <h1>Zuitt Coding Bootcamp</h1>
            <p>Opportunities for everyone, everywhere.</p>

            <Button variant='primary'>ENROLL NOW!</Button>
        </Col>
    </Row>
  )
}

export default Banner