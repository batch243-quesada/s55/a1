import React from 'react'
import { Card, Col, Row } from 'react-bootstrap'

const Highlights = () => {
  return (
    <Row className='mt-3 mb-3'>
        <Col xs={12} md={4}>
            <Card className='cardHighlight p-3'>
                <Card.Body>
                    <Card.Title>
                      <h2>Learn From Home</h2>
                    </Card.Title>
                    <Card.Text>
                         Lorem ipsum, dolor sit amet consectetur adipisicing elit. Iste veniam sit numquam voluptatem doloremque illum labore harum quidem mollitia ad aut nostrum aliquid ex fuga animi suscipit cumque, aliquam dignissimos ducimus rerum enim eos provident soluta dolorem? Culpa esse facere molestias tempore harum sapiente nesciunt autem, corporis, sit sed dolorem.
                    </Card.Text>
                </Card.Body>
            </Card>
        </Col>

        <Col xs={12} md={4}>
            <Card className='cardHighlight p-3'>
                <Card.Body>
                    <Card.Title>
                      <h2>Study Now, Pay Later</h2>
                    </Card.Title>
                    <Card.Text>
                         Lorem ipsum, dolor sit amet consectetur adipisicing elit. Iste veniam sit numquam voluptatem doloremque illum labore harum quidem mollitia ad aut nostrum aliquid ex fuga animi suscipit cumque, aliquam dignissimos ducimus rerum enim eos provident soluta dolorem? Culpa esse facere molestias tempore harum sapiente nesciunt autem, corporis, sit sed dolorem.
                    </Card.Text>
                </Card.Body>
            </Card>
        </Col>

        <Col xs={12} md={4}>
            <Card className='cardHighlight p-3'>
                <Card.Body>
                    <Card.Title>
                      <h2>Be part of our community</h2>
                    </Card.Title>
                    <Card.Text>
                         Lorem ipsum, dolor sit amet consectetur adipisicing elit. Iste veniam sit numquam voluptatem doloremque illum labore harum quidem mollitia ad aut nostrum aliquid ex fuga animi suscipit cumque, aliquam dignissimos ducimus rerum enim eos provident soluta dolorem? Culpa esse facere molestias tempore harum sapiente nesciunt autem, corporis, sit sed dolorem.
                    </Card.Text>
                </Card.Body>
            </Card>
        </Col>
    </Row>
  )
}

export default Highlights