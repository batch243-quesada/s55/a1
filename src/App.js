// modules
import './App.css';
import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';

const App = () => {
  return (
    <>
      <AppNavbar />
      <Home/>
    </>
  );
}

export default App;
