import { Container } from 'react-bootstrap'
import Banner from '../components/Banner'
import Highlights from '../components/Highlights'
import Courses from './Courses'

const Home = () => {
  return (
    <Container className='mb-5 homeContainer'> 
          <Banner />
          <Highlights />
          <Courses/>
    </Container>
  )
}

export default Home