import coursesData from "../data/courses";
import CourseCard from "../components/CourseCard";
import { Col, Row } from "react-bootstrap";

const Courses = () => {
  return (
    <Row>
      {coursesData.map(course => (
        <Col className="courseCard offset-md-4" md={4} key={course.id}>
            <CourseCard course = {course}/>
        </Col>
      ))}
    </Row>
  )
}

export default Courses